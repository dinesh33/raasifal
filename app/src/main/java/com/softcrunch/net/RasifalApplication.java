package com.softcrunch.net;

import android.app.Application;
import android.content.Context;

/**
 * Created by deadlydragger on 9/10/17.
 */

public class RasifalApplication extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        RasifalApplication.context = getApplicationContext();

    }

    public static Context getContext() {
        return RasifalApplication.context;
    }
}
