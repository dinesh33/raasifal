package com.softcrunch.net.fcm;

/**
 * Created by deadlydragger on 9/21/17.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.softcrunch.net.utilis.Pref;


/**
 * Created by deadlydragger on 12/8/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {



    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("dinesh", "onTokenRefresh: "+refreshedToken);
        Pref.setFcm(refreshedToken);

    }

}