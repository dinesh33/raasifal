package com.softcrunch.net.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softcrunch.net.nepalirasifal.R;

/**
 * Created by deadlydragger on 11/27/17.
 */

public class MainFragment extends Fragment implements View.OnClickListener {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.meroRasifal).setOnClickListener(this);
        view.findViewById(R.id.sapanakoFal).setOnClickListener(this);
        view.findViewById(R.id.bibahaJur).setOnClickListener(this);
        view.findViewById(R.id.chinaHeraunuhos).setOnClickListener(this);
        view.findViewById(R.id.mitiPariwartan).setOnClickListener(this);
        view.findViewById(R.id.aajkoBinimaye).setOnClickListener(this);
        view.findViewById(R.id.jyotishSampark).setOnClickListener(this);
        view.findViewById(R.id.hamroBarema).setOnClickListener(this  );
        ((MerorasiFalActivity)getActivity()).goToDetails("","");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment,container,false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.meroRasifal:
                startActivity(new Intent(getActivity(),MerorasiFalActivity.class));
                break;
            case R.id.sapanakoFal:
                startActivity(new Intent(getActivity(),SapanakoFalActivity.class));

                break;
            case R.id.bibahaJur:
                startActivity(new Intent(getActivity(),BibahaJurActivity.class));

                break;
            case R.id.chinaHeraunuhos:
                startActivity(new Intent(getActivity(),ChinaHeraunaActivity.class));

                break;
            case R.id.mitiPariwartan:
                startActivity(new Intent(getActivity(),DateConverterActivity.class));

                break;
            case R.id.aajkoBinimaye:
                startActivity(new Intent(getActivity(),CurrencyConverterActivity.class));

                break;
            case R.id.jyotishSampark:
                startActivity(new Intent(getActivity(),JyotishContactActivity.class));

                break;
            case R.id.hamroBarema:
                startActivity(new Intent(getActivity(),AboutAppActivity.class));

                break;
        }
    }
}
