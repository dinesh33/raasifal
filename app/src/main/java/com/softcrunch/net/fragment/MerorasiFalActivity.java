package com.softcrunch.net.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.softcrunch.net.nepalirasifal.R;
import com.softcrunch.net.nepalirasifal.RasifalDetalsActivity;

/**
 * Created by deadlydragger on 9/9/17.
 */

public class MerorasiFalActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mesh, brish, mithun, karkat, sing, kanya, tula, brischik, dhanu, makkar, kumba, min;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rasifal_fragment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView textView = (TextView) toolbar.findViewById(R.id.title_toolar);
        textView.setText(getString(R.string.mero_rasifal));
        mesh = (LinearLayout)findViewById(R.id.mesh);
        brish = (LinearLayout)findViewById(R.id.brish);
        mithun = (LinearLayout)findViewById(R.id.mithun);
        karkat = (LinearLayout)findViewById(R.id.karkat);
        sing = (LinearLayout)findViewById(R.id.singha);
        kanya = (LinearLayout)findViewById(R.id.kanya);
        tula = (LinearLayout)findViewById(R.id.tula);
        brischik = (LinearLayout)findViewById(R.id.brischik);
        dhanu = (LinearLayout)findViewById(R.id.dhanu);
        makkar = (LinearLayout)findViewById(R.id.maker);
        kumba = (LinearLayout)findViewById(R.id.kumba);
        min = (LinearLayout)findViewById(R.id.min);

        mesh.setOnClickListener(this);
        brish.setOnClickListener(this);
        mithun.setOnClickListener(this);
        karkat.setOnClickListener(this);
        sing.setOnClickListener(this);
        kanya.setOnClickListener(this);
        tula.setOnClickListener(this);
        brischik.setOnClickListener(this);
        dhanu.setOnClickListener(this);
        makkar.setOnClickListener(this);
        kumba.setOnClickListener(this);
        min.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mesh:
                goToDetails("मेष", "चु, चे, चो, ला, लि, लु, ले, लो, अ");
                break;
            case R.id.brish:
                goToDetails("वृष", "इ, उ, ए, ओ, बा, बि, बु, बे, बो");
                break;
            case R.id.mithun:
                goToDetails("मिथुन", "का, कि, कु, घ, ङ, छ, के, को, ह");
                break;
            case R.id.karkat:
                goToDetails("कर्कट", "हि, हु, हे, हो, डा, डि, डु, डे, डो");
                break;
            case R.id.singha:
                goToDetails("सिंह", "मा, मि, मु, मे, मो, टा, टि, टु, टे");
                break;
            case R.id.kanya:
                goToDetails("कन्या", "टो, प, पि, पु, ष, ण, ठ, पे, पो");
                break;
            case R.id.tula:
                goToDetails("तुला", "र, रि, रु, रे, रो, ता, ति, तु, ते");
                break;
            case R.id.brischik:
                goToDetails("वृश्चिक", "तो, ना, नि, नु, ने, नो, या, यि, यु ");
                break;
            case R.id.dhanu:
                goToDetails("धनु", "ये, यो, भ, भि, भु, ध, फा, ढ, भे ");
                break;
            case R.id.maker:
                goToDetails("मकर", "भो, ज, जि, खि, खु, खे, खो, गा, गि");
                break;
            case R.id.kumba:
                goToDetails("कुम्भ", "गु, गे, गो, सा, सि, सु, से, सो, द ");
                break;
            case R.id.min:
                goToDetails("मीन", "दि, दु, थ, झ, ञ, दे, दो, च, चि");
                break;
        }

    }

    public void goToDetails(String rasi, String first_letter) {
        Intent intent = new Intent(getApplicationContext(), RasifalDetalsActivity.class);
        intent.putExtra("rasi", rasi);
        intent.putExtra("first_letter", first_letter);
        startActivity(intent);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
