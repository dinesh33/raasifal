package com.softcrunch.net.nepalirasifal;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.softcrunch.net.fragment.AboutAppActivity;
import com.softcrunch.net.fragment.BibahaJurActivity;
import com.softcrunch.net.fragment.ChinaHeraunaActivity;
import com.softcrunch.net.fragment.CurrencyConverterActivity;
import com.softcrunch.net.fragment.DateConverterActivity;
import com.softcrunch.net.fragment.JyotishContactActivity;
import com.softcrunch.net.fragment.MainFragment;
import com.softcrunch.net.fragment.MerorasiFalActivity;
import com.softcrunch.net.fragment.SapanakoFalActivity;
import com.softcrunch.net.utilis.Utility;

import org.json.JSONObject;

import java.util.Calendar;

public class MainRasiFalActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView textView;
    private Calendar calendar;
    private String toDay;


    //    navigation
    private NavigationView nDrawer;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textView = (TextView) toolbar.findViewById(R.id.title_toolar);

        textView.setText(getString(R.string.app_name));
        setNavigation();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment, new MainFragment())
                .commit();
        FirebaseMessaging.getInstance();

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        new RegisterDevice(android_id).execute();
    }

    public void setNavigation() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nDrawer = (NavigationView) findViewById(R.id.nvView);
        nDrawer.setItemIconTintList(null);
        actionBarDrawerToggle = setupDrawerToggle();
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        setDrawerContent(nDrawer);
        View headerLayout = nDrawer.inflateHeaderView(R.layout.drawable_header_layout);
        actionBarDrawerToggle.syncState();
    }

    private void setDrawerContent(NavigationView nDrawer) {
        nDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                selectDrawerIteam(item);


                return true;
            }
        });
    }

    protected void selectDrawerIteam(MenuItem menuItem) {
        try {
            switch (menuItem.getItemId()) {
                case R.id.nav_home:
//                    loadFragment(MainRasiFalActivity.class);
                    break;
                case R.id.nav_rasi_fal:
                    loadFragment(new MerorasiFalActivity());
                    break;
                case R.id.nav_sapana_fal:
                    loadFragment(new SapanakoFalActivity());
                    break;
                case R.id.nav_marrid_jur:
                    loadFragment(new BibahaJurActivity());
                    break;
                case R.id.nav_future_fal:
                    loadFragment(new ChinaHeraunaActivity());
                    break;
                case R.id.nav_jyotish_contact:
                    loadFragment(new JyotishContactActivity());
                    break;
                case R.id.nav_miti_converter:
                    loadFragment(new DateConverterActivity());
                    break;
                case R.id.nav_exchange:
                    loadFragment(new CurrencyConverterActivity());
                    break;
                case R.id.nav_about:
                    loadFragment(new AboutAppActivity());
                    break;
                default:
            }
            menuItem.setChecked(false);
            setTitle(menuItem.getTitle());
            drawerLayout.closeDrawers();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }


    private ActionBarDrawerToggle setupDrawerToggle() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };


        return actionBarDrawerToggle;
    }

    public class RegisterDevice extends AsyncTask<String, String, String> {
        String android_id;

        public RegisterDevice(String android_id) {
            this.android_id = android_id;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("DeviceCode", android_id);
                jsonObject.put("DeviceType", "AND");
                jsonObject.put("DeviceId", FirebaseInstanceId.getInstance().getToken());
                Log.d(com.softcrunch.net.utilis.TAG.TAG, "doInBackground: " + jsonObject);
                return Utility.postBody("http://merorashiapi.koltintutorial.com/api/Rashi/RegisterDeviceDetails", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }
    }

    public void loadFragment(Activity activity) {
       startActivity(new Intent(MainRasiFalActivity.this,activity.getClass()));

    }
}
