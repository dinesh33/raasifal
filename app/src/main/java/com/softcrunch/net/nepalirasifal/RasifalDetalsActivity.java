package com.softcrunch.net.nepalirasifal;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.softcrunch.net.utilis.Pref;
import com.softcrunch.net.utilis.TAG;
import com.softcrunch.net.utilis.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by deadlydragger on 9/10/17.
 */

public class RasifalDetalsActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private TextView textView, textDaily, date, first_letter;
    private Calendar calendar;
    private String toDay;
    private FrameLayout progressBar;
    private Button daily,weekly,monthly,yearly;
    private String rasifalDaily,weeklyRasifal,monthlyRasi,yearlyRasi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textView = (TextView) toolbar.findViewById(R.id.title_toolar);
        textDaily = (TextView) findViewById(R.id.textDaily);


        date = (TextView) findViewById(R.id.date);
        first_letter = (TextView) findViewById(R.id.first_letter);
        progressBar = (FrameLayout) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        daily=(Button)findViewById(R.id.daily) ;
        weekly=(Button)findViewById(R.id.weekly);
        monthly=(Button)findViewById(R.id.monthly);
        yearly=(Button)findViewById(R.id.yearly);

        daily.setOnClickListener(this);
        weekly.setOnClickListener(this);
        monthly.setOnClickListener(this);
        yearly.setOnClickListener(this);

//        setContent();
        calendar = Calendar.getInstance();

        int month = calendar.get(Calendar.MONTH) + 1;
        toDay = month + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "/" + calendar.get(Calendar.YEAR);
        new GetRasifal().execute();
        setContent();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    public void setContent() {
        try {
            textView.setText(getIntent().getStringExtra("rasi"));
            first_letter.setText(getIntent().getStringExtra("first_letter"));
            JSONObject jsonObject = new JSONObject(Pref.getPref());
            boolean success = jsonObject.getBoolean("success");
            if (success) {
                JSONArray array = jsonObject.getJSONArray("data");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject1 = array.getJSONObject(i);
                   /* if (jsonObject1.getString("RashiName").equalsIgnoreCase(getIntent().getStringExtra("rasi"))) {
                        text_details.setText(jsonObject1.getString("Content"));
                        date.setText(jsonObject1.getString("Date"));
                    }*/

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.daily:
                daily.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button));

                daily.setTextColor(getResources().getColor(R.color.white_text));
                weekly.setTextColor(getResources().getColor(R.color.colorPrimary));
                monthly.setTextColor(getResources().getColor(R.color.colorPrimary));
                yearly.setTextColor(getResources().getColor(R.color.colorPrimary));
                weekly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                monthly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                yearly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                textDaily.setText(rasifalDaily);
                break;
            case R.id.weekly:
                weekly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button));

                weekly.setTextColor(getResources().getColor(R.color.white_text));
                daily.setTextColor(getResources().getColor(R.color.colorPrimary));
                monthly.setTextColor(getResources().getColor(R.color.colorPrimary));
                yearly.setTextColor(getResources().getColor(R.color.colorPrimary));

                daily.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                monthly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                yearly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));

                textDaily.setText(weeklyRasifal);
                break;
            case R.id.monthly:
                monthly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button));

                monthly.setTextColor(getResources().getColor(R.color.white_text));
                daily.setTextColor(getResources().getColor(R.color.colorPrimary));
                weekly.setTextColor(getResources().getColor(R.color.colorPrimary));
                yearly.setTextColor(getResources().getColor(R.color.colorPrimary));

                daily.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                weekly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                yearly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));

                textDaily.setText(monthlyRasi);
                break;
            case R.id.yearly:
                yearly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button));


                yearly.setTextColor(getResources().getColor(R.color.white_text));
                daily.setTextColor(getResources().getColor(R.color.colorPrimary));
                weekly.setTextColor(getResources().getColor(R.color.colorPrimary));
                monthly.setTextColor(getResources().getColor(R.color.colorPrimary));

                daily.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                weekly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));
                monthly.setBackgroundDrawable(getResources().getDrawable(R.drawable.round_background_button_hollo));

                textDaily.setText(yearlyRasi);
                break;
        }
    }


    public class GetRasifal extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("date", toDay);
                jsonObject.put("Rashi",getIntent().getStringExtra("rasi"));
                Log.d(com.softcrunch.net.utilis.TAG.TAG, "doInBackground: " + jsonObject);
                return Utility.postBody("http://merorashiapi.koltintutorial.com/api/rashi/GetAllRashiFal", jsonObject.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG.TAG, "onPostExecute: "+s);
            progressBar.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(s);
                boolean success = jsonObject.getBoolean("success");
                if (success) {
                    JSONObject array = jsonObject.getJSONObject("data");
                   JSONObject DainikRashi = array.getJSONObject("DainikRashi");

                    textDaily.setText(DainikRashi.getString("Content"));
                    rasifalDaily=DainikRashi.getString("Content");


                    JSONObject Sapthahik = array.getJSONObject("Sapthahik");
                    JSONObject Masik = array.getJSONObject("Masik");

                    JSONObject Barsik = array.getJSONObject("Barsik");

                    weeklyRasifal=Sapthahik.getString("Content");

                    monthlyRasi=Masik.getString("Content");
                    yearlyRasi=Barsik.getString("Content");
                }


            }catch (Exception e){
                e.printStackTrace();
            }
//            Pref.setPref(s);
        }
    }
}
