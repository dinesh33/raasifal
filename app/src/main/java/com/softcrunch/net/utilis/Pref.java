package com.softcrunch.net.utilis;

import android.content.Context;
import android.content.SharedPreferences;

import com.softcrunch.net.RasifalApplication;

/**
 * Created by deadlydragger on 9/10/17.
 */

public class Pref {
    public static void setPref(String pref) {
        SharedPreferences sharedPreferences = RasifalApplication.getContext().getSharedPreferences("rasifal", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("rasifal", pref);
        editor.apply();
    }

    public static String getPref() {
        SharedPreferences sharedPreferences = RasifalApplication.getContext().getSharedPreferences("rasifal", Context.MODE_PRIVATE);
        return sharedPreferences.getString("rasifal", "");
    }

    public static void setFcm(String pref) {
        SharedPreferences sharedPreferences = RasifalApplication.getContext().getSharedPreferences("rasifal", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fcm", pref);
        editor.apply();
    }

    public static String getFcm() {
        SharedPreferences sharedPreferences = RasifalApplication.getContext().getSharedPreferences("rasifal", Context.MODE_PRIVATE);
        return sharedPreferences.getString("fcm", "");
    }
}
