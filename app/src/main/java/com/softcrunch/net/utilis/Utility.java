package com.softcrunch.net.utilis;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by deadlydragger on 6/28/17.
 */

public class Utility {
    public String getFromurl(String url) {
        String result = "";
        try {
            Log.d(TAG.TAG, "getFromurl: " + url);
            OkHttpClient okHttpClient = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            result = response.body().string();
        } catch (Exception e) {
            e.printStackTrace();
            e.printStackTrace();

        }

        return result;
    }

    public static  String postBody(String url, String s) throws IOException {
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        OkHttpClient client = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, s);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();

    }
    public void  setTipsData(Context context, String tipsData){
        SharedPreferences sharedPreferences = context.getSharedPreferences("beautytips",Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putString("beautytips",tipsData);
        editor.apply();
    }
    public String getTipsDate(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("beautytips",Context.MODE_PRIVATE);
        String tip = sharedPreferences.getString("beautytips","");
        return tip;
    }


}
